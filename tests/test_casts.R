library(testthat)

source("./R/mm.R")

test_that("casts.current_time_vector", {
  expect_equal(sum(current_time(2)), as.numeric(Sys.time())*2)
  expect_error(current_time(2,2))
  expect_error(current_time("i'm_a_typo_Integer"))
})

test_that("casts.as_unix_ts", {
  expect_length(as_unix_ts(c("1970-01-01T00:01:01","1970-01-01T00:00:01")), 2)
  expect_equal(sum(as_unix_ts(c("1970-01-01T00:00:01","1970-01-01T00:00:01","1970-01-01T00:00:01"))), 3)
  expect_equal(sum(as_unix_ts(c("1970-01-01T00:01:01","1970-01-01T00:00:01","1970-01-01T00:00:01"))), 63)
})

test_that("casts.as_year", {
  expect_length(as_year(c("1970-01-01T00:01:01","1970-01-01T00:00:01")), 2)
  expect_equal(as_year(c("1971-01-01T00:01:01","1970-01-01T00:00:01","1970-01-01T00:00:01")), c("1971", "1970", "1970"))
})

test_that("casts.as_month", {
  expect_equal(as_month(c("1971-01-01T00:01:01","1970-12-01T00:00:01","1987-10-01T00:00:01")), c("1971-01", "1970-12", "1987-10"))
})

test_that("casts.as_quarter", {
  expect_equal(as_quarter(c("1971-01-01T00:01:01","1970-12-01T00:00:01","1987-06-30T00:00:01")), c("1971Q1", "1970Q4", "1987Q2"))
})

test_that("casts.as_week", {
  expect_equal(as_week(c("1971-01-01T00:01:01","1970-12-01T00:00:01","1987-10-01T00:00:01")), c("1971w00", "1970w48", "1987w39"))
})

test_that("casts.as_day", {
  expect_equal(as_day(c("1971-01-01T00:01:01","1970-12-02T11:00:01","1987-10-10T00:00:01")), c("1971-01-01", "1970-12-02", "1987-10-10"))
})

test_that("casts.as_weekday", {
  expect_equal(as_weekday(c("1971-01-01T00:01:01","1970-12-02T11:00:01","1987-10-10T00:00:01")), c("Freitag", "Mittwoch", "Samstag"))
})

test_that("casts.as_days_from", {
  expect_equal(as_days_from(c("1986-10-10T00:00:01","1986-10-11T00:00:01","1987-10-10T00:00:01"), "1986-10-10"), c(0,1,365))
})
