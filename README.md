# openCQA notebook
openCQA is an open source tool for **C**ollaborative knowledge-based healthcare data **Q**uality **A**ssessment. This project strives to provide easy MM-execution and some other functionalities for supporting health data quality assessment employing the interface of a computational notebook, i.e. the R Notebook. 

The central idea is to base data quality assessment on shareable and machine-actionable definitions for methods that quantify or visualize characteristics of datasets. I call these definitions measurement methods (MM). Each MM is defined by five elements, 1. some descriptive Tags, 2. Domain paths/variable names specifying the intended input data for the MM, 3. an optional Check function, 4. an optional Grouping function and 5. a Characterization function that creates the final output quantifying or visualizing the characteristic of interest. A simple example: We want to know the rate of pulse values outside a given range for each patient in a dataset. A MM-definition and the code to run it in an R Notebook, is as simple as the following. 

``` 
mm_check_pulse_range = mm("check,pulse_range,rate",
   "pulse, pat_id",
   "mean(df$checked)", 
   'pulse < 40 || pulse > 300',
   "pat_id"
)
run(mm_check_pulse_range)
```

Of course, we can create plots and reports or make use of R-packages, since we are basically just using R. And we can combine MMs in multiple-layers, e.g. to use the results of other MMs. 

MMs are intended for collaborative governance/management. The knowledge which MMs to apply in which use cases for which data and how to prioritize and assess MM-results should not base on the opinion of single experts, but rather on collaboratively governed knowledge. A focus of my research activities in the last years was to develop, evaluate and refine concepts to support this idea. The practical solution envisaged here is to assemble the necessary MMs for different use cases as respective R Notebooks, together with descriptive/explaining text and the necessary code to extract the data (which obviously could need to be adapted when sharing such a notebook with other sites). We could call such a compilation of MMs a DQ knowlege base.

## Dashboards
Of course, we can integrate MMs into, e.g. Shiny dashboards. An own project implementing a simple Shiny-dashboard making use of MMs can be found [here](https://gitlab.gwdg.de/erik.tute/cardio-sensorik-dashboard). If you do not have clear instructions/ideas about what the dashboard should display, it seems to be a good way to identify the informative values and plots by first playing around with a notebook. If you are happy with what you got in your notebook, it is pretty straightforward to transfer suitable MMs (and data retrieval) into your dashboard.  

# Getting started
One of the main reasons for moving openCQA to computational notebooks is to ease the rollout. All prerequisites you need is a current installation of ``RStudio`` (I assume you know about ``R`` for statistical computing, https://www.r-project.org). Now you can download the R-files and you are ready to run any notebooks containing MMs. You could start with the example notebook provided in this repository.

# License
MIT License

# Project status
I started to move *openCQA* to R Notebook in late 2023. It is under active development, driven by my own needs. This state already allows you to run MMs, to create your own notebooks and supports you with the most basic tasks, e.g. to simply generate some MMs based on the datatypes of the variables in your dataset. 

## Beginnings of openCQA 
The [first implementation of openCQA as node.js applicaton](https://gitlab.gwdg.de/erik.tute/opencqa) was part of my thesis and its main purpose was to show that the theoretical idea of formalizing knowledge on DQ-assessment works and to show how certain details could be solved. Since the first steps in 2018, during a few real world applications and through participation in data quality related working groups, I learned some things. This lead to moving the implementation of *openCQA* to computational notebooks in order to ease usage for others.  

# Using openCQA

## Retrieving data 
MMs expect the input data to be in variables in the global environment (each variable containing its data as string-, numeric- or boolean-vector). The ``example notebook.Rmd`` simply creates the dummy-data in the R-script, which is fine to understand MM-basics. Of course, in real world use we want to use data from files, databases or APIs. R supports this well and there is nothing special about retrieving the data for MMs. Simply do it in your Notebook before running the MMs.

### Retrieving data from an openEHR data repository
In the notebook ``MeDIC Explorer.Rmd`` you can see how a few lines of R-code suffice to retrieve and use data from a data repository complying with the *openEHR REST-API specification*.    

## Creating and running MMs

As shown above you create a MM by calling the mm-constructor. You have to specify at least the tags, the input variables and the characterization function. 
``` 
my_mm = mm("pulse,mean",
   "pulse",
   "mean(pulse, na.rm = TRUE)" 
)
```
Execute the MM using the ``run`` command.
``` 
run(my_mm)
```

### Tags
The tags are descriptive keywords intended to briefly indicate the MM's purpose to a human user.

### Domain paths/variable names
Define the input data for the MM. Since openCQA on R Notebook this is simplified within the MM. Simply state the names of the variables containing the needed data, e.g. ``"pulse, patient_id, date_of_visit"``. 

Each notebook should have a section, where variables are filled with data from a data source. But the input-variables for MMs can also contain data resulting from other MMs, e.g.: 
```
pulse_check_rates = as.numeric(mm_res$value)
add_dp("pulse_check_rates", "Real", list(result_of = "mm_check_pulse_rate.value") )
```
The user, i.e. the person creating the respective notebook, is responsible for ensuring, that result variables and input data for multi-layered MMs fit together.  

Furthermore, each notebook should contain additional information unambiguously specifying in a machine-actionable way (preferably making use of some standard), what data is expected in the respective variables used as input data for the MMs. You can check the example notebook and look how the ``domain paths`` are handled there. Basically, there is a list ``domain_paths`` in the global environment, which contains this information. Each variable that is used by MMs should at least be listed there together with its datatype (do not confuse these with R datatype). You can add information to ``domain paths`` using a convenience function, e.g.:

```
add_dp("start_time", "ISO8601", list(openEHR_archetype_path = "c/context/start_time/value") )
```
(variable name, datatype, list containing additional information)

### Characterization function
The R-code actually producing the desired MM-results, e.g. calculating the mean
``` 
"mean(df$pulse, na.rm = TRUE)" 
```
or creating a boxplot.
``` 
"boxplot(pulse)"
```
In *R* most plotting functions also create some stats. You can add them to the results, by including ``#show stats`` in your characterization function.
```
"boxplot(pulse)
#show stats
"
```

If you apply a grouping in your MM, the *characterization* will be applied for each group.

You can access the input variables by their names or by ``df$`` + variable name. The ``df$`` convention allows you to access each input variable as well as the results of the check (``df$checked``) and grouping (``df$grouping``) functions within the characterization for each group in the data. 

### Check function
The *Check* function is optional. It specifies a rule which is applied to each row of the input data. You can access the results within the *Characterization* as ``df$checked``. For example, if we would like to check, whether our data is within a given range we simply state something like:

``"pulse < 40 || pulse > 300"``


### Grouping function
The *Grouping* function is optional. If you are not interested in results for the whole dataset, but in the results for subsets, e.g. for each hospital or for each patient, this is the way to go. You can think of it as something similar to the *GROUP BY* in SQL. For example, the following *MM* will calculate the rate of pulse values outside the range 40-300 for each patient. 
``` 
mm("check,pulse_range,rate",
   "pulse, pat_id",
   "mean(df$checked)", 
   'pulse < 40 || pulse > 300',
   "pat_id"
)
```

## Convenience functions
Running MMs in R Notebooks is the core. But obviously there are some common tasks, which should be as easy as possible. This is why *openCQA* provides some convenience functions.

### Generating MMs
 
#### Based on datatype (mms_per_datatype.R)
There is a convenience function to derive code for MMs for a given variable. For example,
```
generate_mms("variable_name")
```
will generate as output some MMs (e.g. calculating things like min, max, mean, meadian, ...) which are suitable for the datatype of the variable (based on ``domain_paths``). You can also specify a datatype. You can (copy-)paste these MMs into your notebook and are ready to start exploring your data.  

Currently there are proposed MMs available for datatypes ``"String","Code_string","Id_issuer","Id_assigner","Id_type","Id","Boolean","Integer","Real","ISO8601","ISO8601_duration"``

We can also generate MMs for all variables in our ``domain_paths``:

```
generate_mms_for_all_vars()
```

#### Adding groupings in certain dimensions (dimensions.R)
There is a convenience function to derive code for a MM with a grouping for a certain dimension added.
```
add_dimension(mm_to_extend, 
dimension_name, 
optional_the_variabel_containing_the_data_for_this_dimension, 
optional_rule_how_to_group
```
If dimension_name is one of: ``per_site, per_composer, per_patient, per_year, per_quarter, per_month, per_week, per_day, per_day_of_week, per_time_interval`` a default variable name and grouping rule is available.

Using this function simply looks like this and returns the code for a MM containing the respective grouping:
```
add_dimension(mm1, "per_site")
```

We can also pass these groupings to the generation per datatype, e.g.
```
generate_mms("pulse", add_a_dimension = c("per_site"))
#generate_mms_for_all_vars(FALSE, c("per_site"))
#generate_mms_for_all_vars(FALSE, c("per_planet", "planet_names", "planet_names!='earth'")) # as with the add_dimension we can specify our own rules for dimensions. 
```

### Casts (casts.R)
There are some convenience functions to ease common (mainly) cast operations to have your variables with input data as needed. 

To have a vector holding the current time as UNIX timestamp
``` current_time(optional_numeric_length_of_vector)```

To convert ISO8601-Date-Time-values of a vector into UNIX timestamps.
``` as_unix_ts(input_vector)```

To convert ISO8601-Date-Time-values of a vector into year strings, e.g. 2023
```as_year(input_vector)```

To convert ISO8601-Date-Time-values of a vector into year-month strings, e.g. 2023-11
```as_month(input_vector)```

To convert ISO8601-Date-Time-values of a vector into year-quarter strings, e.g. 2023Q3
```as_quarter(input_vector)```

To convert ISO8601-Date-Time-values of a vector into year and week strings, e.g. 2023w11
```as_week(input_vector)```

To convert ISO8601-Date-Time-values of a vector into a year-month-day string, e.g. 2023-11-02
```as_day(input_vector)```

To convert ISO8601-Date-Time-values of a vector into weekday strings, e.g. Monday
```as_weekday(input_vector)```

To convert ISO8601-Date-Time-values of a vector into the difference in days between the datetime and a given date of interest (specified as ISO8601 date string)
```as_days_from(input_vector, date_of_interest)```
